# Woche 2

## Ziele

- [x] Umgebung aufschalten 
    - Verstehen
    - einlesen

- [x] GitLab erstellen
    - Ordner erstellen
    - Dokumentation

## Reflexion 

Nachdem ich mich in WordPress eingearbeitet hatte, begann ich, mich mit den Anforderungen für das Hosting einer Website zu befassen. Dazu gehörten Aspekte wie die Auswahl eines geeigneten Webhosts, die Überlegung von Speicherplatz- und Bandbreitenanforderungen sowie die Bewertung von Sicherheitsfunktionen und Supportoptionen. 

Insgesamt war es ein aufschlussreicher Tag, der mir half, die grundlegenden Schritte für die Erstellung und das Hosting einer Website besser zu verstehen.

## Nächstes Mal?

Das nächste mal werde ich versuchen mit `tbz.hopto.org` dort einige Einstellungen zu machen und versuchen zu migrieren-
