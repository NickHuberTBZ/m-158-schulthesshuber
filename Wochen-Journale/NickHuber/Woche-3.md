# Woche 3

## Ziele

- [x] Aufgabe UNC-Pfade

- [x] Plugin für Migration kennen lernen

- [x] PLanung schreiben

## Reflexion 

Am Morgen hatten wir eine Einführung zu UNC-Pfade, danach haben wir noch eine Übung dazu gemacht. Dieses Übung werde ich danach in unser Theorie Folder hochladen. 

Nach dieser Übung haben wir mit unserer Planung begonnen wir haben einfach nach Arten gesuscht. Wie wir migrieren können, dabei haben wir ein Plugin gefunden für Wordpress wie man dies all in-one migrieren kann.

## Nächstes Mal?

- Planung schreiben

- Plugin versuchen.