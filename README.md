# M159 - Directoryservices konfigurieren und in Betrieb nehmen

**Linkliste**

- [Projekt](Projekt)
- [Theorie](Theorie)
- [Wochen-Journale](Wochen-Journalen) 

---

Herzlich willkommen im Modul 159, in diesem Modul geht es um migrieren von Plattformen! 

<img src="DirectoryMigrate.png" alt="DirectoryMigrate.png" width="400" height="300">

## Version der Modulidentifikation

[Modulbaukasten ICT-BCH](https://www.modulbaukasten.ch/module/159/3/de-DE?title=Directoryservices-konfigurieren-und-in-Betrieb-nehmen), hier finden sie eine genauere Beschreibung zu diesem Modul.




