# Dokumentation Migrationsprojekt M158

[TOC]

## Auftrag
Für das Migrationsprojekt M158 haben wir uns entschieden, eine WordPress-Seite gemäß den Anweisungen im [Modul-Repo](https://gitlab.com/ch-tbz-it/Stud/m158/-/tree/main/02_Projekt%20Migration/WordPress-Web-App-Migration) zu migrieren. Allerdings haben wir beschlossen, nicht die von der TBZ bereitgestellte Proxmox-VM zu verwenden. Stattdessen haben wir eine kleine VPS-Instanz gemietet und die Webseite darauf migriert. Sie ist erreichbar unter: [wp-toor.m158.fisch.one](https://wp-toor.m158.fischz.one).

## Ziele des Projekts

*   [x] Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert
*   [x] Die Datenbank von der Quellinstallation wurde migriert
*   [x] Es wurden neuen Datenbankbenutzer angelegt und entsprechende Berechtigungen vergeben
*   [X] Es wurde ein FTP-Dienst für das Übertragen der Dateien eingerichtet
*   [X] Ein FTP-User, der nur das Root-Verzeichnis der Website sieht, wurde eingerichtet
*   [X] Ein Webserver wurde eingerichtet
*   [X] Der Webserver läuft unter einem virtuellen Host Ihrer Wahl
*   [X] HTTP wird auf HTTPS umgeleitet inkl. TLS-Zertifikat
*   [X] Es wurde PHP installiert und aktiviert
*   [X] Es wird min. PHP Version 8.2 eingesetzt
*   [X] Die Website sieht optisch identisch wie die Quellapplikation aus und zeigt keine Fehlermeldungen an
*   [x] Die Seite [https://wp-toor.m158.fisch.one/wp-admin/site-health.php](https://wp-toor.m158.fisch.one/wp-admin/site-health.php) zeigt keine Fehler
*   [x] Die Seite [https://wp-toor.m158.fisch.one/wp-admin/admin.php?page=et\_support\_center\_divi](https://wp-toor.m158.fisch.one/wp-admin/admin.php?page=et_support_center_divi) zeigt keine Fehler auf
*   [X] Ein Datei- und Datenbank-Backup ist serverseitig eingerichtet

## Umsetzung

### Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert

Zusammen mit Apache2 und einigen PHP-Modulen haben wir den MySQL-Server (MariaDB) über `apt` installiert:
```
sudo apt install apache2 ghostscript libapache2-mod-php mysql-server php php-bcmath php-curl php-imagick php-intl php-json php-mbstring php-mysql php-xml php-zip
```
Die Datenbank wurde gemäß den Angaben in der Datei "wp-config.php" konfiguriert, die im Export des Legacy-Servers enthalten war:
```
sudo mysql -u root
```
```
CREATE DATABASE wp_m158_db;

CREATE USER wp_m158_user@localhost IDENTIFIED BY '0Cc7j_6ofbrUFef';

GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, ALTER
    -> ON wp_m158_db.*
    -> TO wp_m158_user@localhost; 
FLUSH PRIVILEGES;
```
```
systemctl restart mysql
```

### Die Datenbank von der Quellinstallation wurde migriert

Mittels des Plugins [All-in-One WP Migration](https://servmask.com/) haben wir auf dem [Legacy Server](https://m158.geekz.ch/) die gesamte Datenbank sowie alle WordPress-Konfigurationen und Einstellungen exportiert. Auf dem Zielserver haben wir nach einer Standardinstallation von WordPress dieselben Daten mittels desselben Plugins importiert. Dabei wurden alle Domänenlinks entsprechend unserer neuen Domain angepasst.

![bildImport](Bilder/websiteImport.png)

### Es wurden neuen Datenbankbenutzer angelegt und entsprechende Berechtigungen vergeben
Siehe [Ein Datenbankserver wurde auf Linux erfolgreich installiert und konfiguriert](#L31).

### Es wurde ein FTP-Dienst für das Übertragen der Dateien eingerichtet

Wir haben das vsftpd-Paket über `apt` installiert:
```
apt install vsftpd
```
Anschließend haben wir die Konfiguration wie folgt angepasst:
```
# FTP-User hat nur Zugriff auf sein Home-Verzeichnis.
chroot_local_user=YES

# FTP-User hat Schreibrechte im Verzeichnis.
allow_writable_chroot=YES

# Verweis auf TLS-Zertifikat für FTPs-Verbindung.
rsa_cert_file=/etc/letsencrypt/live/wp-toor.m158.fisch.one/fullchain.pem
rsa_private_key_file=/etc/letsencrypt/live/wp-toor.m158.fisch.one/privkey.pem
ssl_enable=NO 
```

![FTP](Bilder/certificateWebsite.png)

Der FTP-User wurde der `www-data` Gruppe hinzugefügt, um Zugriff auf das DocumentRoot des Webservers zu erhalten. Das Home-Verzeichnis des FTP-Users wurde auf das Website-Verzeichnis gelegt, um mittels Chroot-Zugriff nur dieses Verzeichnis zugänglich zu machen.

### Ein FTP-User, der nur das Root-Verzeichnis der Website sieht, wurde eingerichtet

Siehe: [Es wurde ein FTP-Dienst für das Übertragen der Dateien eingerichtet](#es-wurde-ein-ftp-dienst-für-das-übertragen-der-dateien-eingerichtet).

Wir haben einen FTP-User eingerichtet, der nur das Root-Verzeichnis unserer Website sehen kann. Dadurch wird sichergestellt, dass dieser Benutzer nur auf die für die Website relevanten Dateien zugreifen kann, ohne Zugriff auf andere sensible Systembereiche zu haben. Diese Maßnahme erhöht die Sicherheit und Integrität unserer Website, während gleichzeitig die Zusammenarbeit und der Dateiaustausch erleichtert werden.

### Ein Webserver wurde eingerichtet

Wir haben uns für Apache2 als Webserver entschieden. Nach der Installation verschiedener Pakete, einschließlich Apache2, haben wir das Standard-WordPress-Paket in `/srv/www` entpackt:

```bash
sudo mkdir -p /srv/www

sudo chown www-data: /srv/www

curl https://wordpress.org/latest.tar.gz | sudo -u www-data tar zx -C /srv/www
```

Wir haben eine neue vhost-Datei (`wordpress.conf`) in `/etc/apache2/sites-available/` erstellt, um unsere Domain zu definieren und später über HTTP auf WordPress zugreifen zu können. Als DocumentRoot haben wir `/srv/www/wordpress` festgelegt, wo WordPress installiert wird.

![bildConfigWordpress](Bilder/wordpressConf.png)

Mittels `sudo a2ensite wordpress` wurde die Webseite aktiviert. Nun konnten wir mit dem Aufruf unserer Domain über den Wordpress Wizard eine Standardinstallation durchführen.

Um den Upload der Exportdatei von unserem Legacy Server via dem Plugin zu ermöglichen, mussten einige Konfigurationen hinsichtlich der maximalen Upload-Größe angepasst werden. Nachdem dies erfolgt ist, kann die Sicherungsdatei vom Legacy Server via demselben Plugin auf unseren neuen Server hochgeladen werden, und das Plugin erledigt sämtliche Migrationsarbeiten.

In der `.htaccess`-Datei:
```
php_value upload_max_filesize 512M
php_value post_max_size 512M
php_value memory_limit 512M
php_value max_execution_time 350
php_value max_input_time 350
```

In der `wp-config.php`-Datei:
```
@ini_set( 'upload_max_filesize' , '512M' );
@ini_set( 'post_max_size', '512M');
@ini_set( 'memory_limit', '512M' );
@ini_set( 'max_execution_time', '350' );
@ini_set( 'max_input_time', '350' );
```


### Webserver läuft unter einem virtuellen Host Ihrer Wahl

Unter Apache2 wird immer auf "virtuelle Hosts" bzw. "vhosts" zurückgegriffen. Im Ordner `/etc/apache2/sites-available/` legen wir .conf-Dateien ab, in welchen wir auf unserem Webserver mehrere Domains und jeweils eigene Root-Verzeichnisse definieren können.

### HTTP wird auf HTTPS umgeleitet inkl. TLS-Zertifikat

Ein selfsigned Certificatae erstellt und dieses wurde dann verwendet.

![BildCertificate](Bilder/certificateViewer.png)

### Es wurde PHP installiert und aktiviert

Wir haben PHP 8.2 aus den Standard-Ubuntu-Paketquellen installiert und entsprechend unter Apache2 auch aktiviert:

```shell
sudo apt install php8.2 

sudo apt install php8.2-mysql php8.2-bcmath php8.2-curl php8.2-imagick php8.2-dom php8.2-zip php8.2-intl
a2dismod php8.1

sudo a2enmod php8.2
```

### Die Website sieht optisch identisch wie die Quellapplikation aus und zeigt keine Fehlermeldungen an

 ![fertige-Seite](Bilder/doneWebsite.png)

### Die Seite [https://wp-toor.m158.fisch.one/wp-admin/site-health.php](https://wp-toor.m158.fisch.one/wp-admin/site-health.php) zeigt keine Fehler     

![Site Health](Bilder/websiteZustand.png)

### Die Seite [https://wp-toor.m158.fisch.one/wp-admin/admin.php?page=et\_support\_center\_divi](https://wp-toor.m158.fisch.one/wp-admin/admin.php?page=et_support_center_divi) zeigt keine Fehler auf

![Site Health](Bilder/websiteStatus.png)

### Ein Datei- und Datenbank-Backup ist serverseitig eingerichtet

Das Plugin "BackWPup", welches bereits auf dem Legacy-Server installiert war, haben wir ebenfalls auf unserer WordPress-Seite installiert und einen Auftrag eingerichtet, der regelmäßig ausgeführt wird, um ein Backup der gesamten Webseite und Datenbank zu erstellen.

![Backup](Bilder/BackwupFertig.png)


