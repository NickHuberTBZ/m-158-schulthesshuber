## Berechtigungen in Windows


### Vererbungskette erkennen

- "Elben", "Menschen" und "Zwerge" erben standardmäßig ihre Berechtigungen von "Völker".
- "Völker" erbt standardmäßig seine Berechtigungen von "Mittelerde".
- "Mittelerde" erbt seine Berechtigungen von seinem übergeordneten Ordner (normalerweise das Laufwerk, auf dem es sich befindet).

Wenn Sie die Vererbung für "Mittelerde" deaktivieren, werden die Berechtigungen für "Völker" nicht mehr von "Mittelerde" geerbt. Aber die Vererbung für "Elben", "Menschen" und "Zwerge" bleibt intakt, da sie von "Völker" erben.

### Auswirkungen beim deaktivieren von Vererbungen verstehen

Um die Gruppe "Menschen" bei "Elben" und "Zwerge" zu entfernen, müssen Sie:

- Die Eigenschaften des jeweiligen Ordners öffnen.
- Zum Tab "Sicherheit" gehen.
- Die Gruppe "Menschen" aus der Liste der Berechtigungen entfernen.

Wenn Sie die Vererbung auf einem Ordner ausschalten, erhalten Sie normalerweise zwei Optionen:

1. **Berechtigungen von übergeordnetem Objekt übernehmen**: Damit werden die Berechtigungen des aktuellen Ordners durch die Berechtigungen des übergeordneten Ordners ersetzt.
2. **Aktuelle Berechtigungen behalten**: Damit bleiben die aktuellen Berechtigungen des Ordners unverändert, und die Vererbung wird deaktiviert.

Im GUI von Windows können Sie sehen, von wo ein Ordner seine Berechtigungen erbt, indem Sie die Eigenschaften des Ordners öffnen und zum Tab "Sicherheit" gehen. Dort finden Sie die Option "Erweitert", wo die aktuelle Vererbungskette aufgelistet ist.

Die Vererbung bei "Mittelerde" zu deaktivieren reicht nicht aus, um die Gruppe "Menschen" bei "Elben" und "Zwerge" zu entfernen, weil "Elben" und "Zwerge" ihre Berechtigungen direkt von "Völker" erben, nicht von "Mittelerde". Sie müssen die Berechtigungen für "Elben" und "Zwerge" direkt ändern, um die Gruppe "Menschen" zu entfernen.