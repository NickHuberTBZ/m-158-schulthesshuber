# Übungen CMS-DB Connection

Verstehen Sie, wie und wann CMS-Applikationen eine Verbindung zu Datenbanken herstellen und wie der Ablauf vom Aufruf einer Webanwendung bis zur Datenbankabfrage ist.



## Übung 1

![BildÜbung](LosungSuper-CMS.png)

## Übung 2

Überlegen Sie wo die folgenden Technologien/Protokolle in der Grafik im Einsatz sind

- https

  Schritte: 1 und 10

- mysqli

  Schritte: 6 und 7

- ext4

  Schritte: 2,3,4,5,8,9

und tragen Sie dies bei den passenden Verbindungen ein.



## Übung 3

Passen Sie die DB-Config Datei auf die Umgebung aus dem Bild an. Speichern Sie die angepasste Config-Datei ins Repository

```php
<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database

	'connectionString' => 'mysql:host=igezoper.mysql.db.internal;dbname=igezoper_mullerPartnersCRM',
	'emulatePrepare' => true,
	'username' => 'igezoper_Muller',
	'password' => '2g*k+7kFMkxNoYimcZ2y',
	'charset' => 'utf8',



);
```
